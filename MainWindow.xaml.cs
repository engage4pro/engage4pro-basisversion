﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//
// Code added by Dominik Bonin is marked with DB in the summary
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.BodyBasics
{
    using Microsoft.Kinect;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
     /// colorBitmap to display
        /// </summary>
        private WriteableBitmap colorBitmap = null;
                       
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 15;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 5;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;
               
        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;
        
        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<Pen> bodyColors;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

              
        /// <summary>
        /// DB Array List to Count Capture Data
        /// </summary>
        ArrayList Capture_XYZCount = new ArrayList();
        ArrayList Capture_DynCount = new ArrayList();
        ArrayList Capture_OwasCount = new ArrayList();
        ArrayList Capture_AllCount = new ArrayList();
        ArrayList Capture_FrameCount = new ArrayList();
        
       

        // String erste Zeile des Arrays
        public String Erste_Zeile_OWAS = "Datum" + " " + "Uhrzeit" + " " + "Owas_Code" + " " + "MK" + " " + "Verbesserungsvorschlag";

        //DB für Ausgabe des OWAS_Code
        string OWAS_Code = "";
        string OWAS_MK = "";
        string OWAS_Verb = "";
        
        string zeitstempel = "";
        string datum = "";        
        
                //  DB Variablen zur Steuerung der Aufnahme und OWAS Codierung
        int XYZ_aufzeichnen = 0;
        int dyn_aufzeichnen = 0;
        int OWAS_aufzeichnen = 0;
        int All_aufzeichnen = 0;
        int OWAS_1 = 0;
        int OWAS_2 = 0;
        int OWAS_3 = 0;
        int OWAS_4 = 0;
        int Sitzen = 0;
        int Vorschläge = 0;
        

                    
        // DB Standard Wert für Speicherort, kann von Standard_Speicherort_Methode überschrieben werden 
        string save_path = Directory.GetCurrentDirectory() ;

        // DB String mit aktuellem Verzeichnis in der das Programm ausgeführt wird
        string current_directory = Directory.GetCurrentDirectory();

       

        // DB für Winkelberechnung
        Double koerperbeugung;
        Double koerperneigung;
        Double koerperrotation;

        Double kniewinkel_links;
        Double kniewinkel_rechts;

        Double ellenbogenwinkel_rechts;
        Double ellenbogenwinkel_links;

        Double Neigungswinkel;
        Double Rollwinkel;

        //DB für CSV Export
        CsvExport Owas_export = new CsvExport();
        CsvExport Gelenkwinkel_export = new CsvExport();
        CsvExport Position_export = new CsvExport();


        //DB Dictionary Maßnahmenklassen
        Dictionary<string, int> dict = new Dictionary<string, int>();

        //DB Dictionary Verbesserungsvorschläge
        Dictionary<string, string> dict2 = new Dictionary<string, string>();

        //DB für Color Stream
        MultiSourceFrameReader _reader;
        CameraMode _mode = CameraMode.Color;

        //DB für Transformation + 
        public Matrix3D trans;
        public Vector4 floorClipPlane = new Vector4();
        

        //DB für Umschalter zwischen Frames
        enum CameraMode
        {
            Color,
            Depth,
            Infrared
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //DB Color Multiframe Reader initialisieren
            _reader = kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
            _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;

            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;
            }
            
            // Standard Speicherort auslesen + Anzeigen
            Speicherort_Textblock.Text = save_path;
           
         }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (_reader != null)
            {
                _reader.Dispose();
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        public void StandardSpeicherort_Methode()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.Description = "Bitte standard Speicherort auswählen \r\n Ein Unterordner Sicherung wird automatisch in dem gewählten Ordner erstellt";
            dialog.SelectedPath = @current_directory;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            string foldername = dialog.SelectedPath;
            save_path = foldername.ToString();

            string sicherungen_path = save_path + "\\Sicherung\\";

            if (!Directory.Exists(sicherungen_path))
            {
                System.IO.Directory.CreateDirectory(sicherungen_path);
            }
        }








    }
}
