﻿namespace Microsoft.Samples.Kinect.BodyBasics
{
    using Microsoft.Kinect;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        # region buttons
        private void Start_Click(object sender, RoutedEventArgs e)
        {
           
            Start_XYZ_Methode();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
           Stop_XYZ_Methode();
           Capture_Count_STOP_Methode();
        }

        private void DynStart_Click(object sender, RoutedEventArgs e)
        {
            DynStart_Methode();
            
        }

        private void DynStop_Click(object sender, RoutedEventArgs e)
        {
            DynStop_Methode();
            Capture_Count_STOP_Methode();
        }

        private void OwasStart_Click(object sender, RoutedEventArgs e)
        {
            
            OwasStart_Methode();            
        }

        private void OwasStop_Click(object sender, RoutedEventArgs e)
        {
            OwasStop_Methode();
            Capture_Count_STOP_Methode();
        }

        public void Alle_aufnehmen_start_Click (object sender, RoutedEventArgs e)
        {
            All_aufzeichnen = 1;
            
            Start_XYZ_Methode();
            DynStart_Methode();
            OwasStart_Methode();
        }

        private void Alle_aufnehmen_stop_Click (object sender, RoutedEventArgs e)
        {
            All_aufzeichnen = 0;
            Stop_XYZ_Methode();
            DynStop_Methode();
            OwasStop_Methode();
            Capture_Count_STOP_Methode();
        }

        private void Checkbox_Sitzen_Checked(object sender, RoutedEventArgs e)
        {
            Sitzen = 1;
        }

        private void Checkbox_Sitzen_Unchecked(object sender, RoutedEventArgs e)
        {
              Sitzen = 0;
        }

        private void CheckBox_Verb_Checked(object sender, RoutedEventArgs e)
        {
            Vorschläge = 1;
        }

        private void CheckBox_Verb_Unchecked(object sender, RoutedEventArgs e)
        {
            Vorschläge = 0;
            Verb_Info.Text = "";
        }

        private void SelectFolder_btn_Click(object sender, RoutedEventArgs e)
        {
            StandardSpeicherort_Methode();
            Speicherort_Textblock.Text = save_path;
        }

        private void Owas_ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int selectedIndex = Owas_ComboBox.SelectedIndex;
            if (selectedIndex == 0)
                OWAS_4 = 1;
            else if (selectedIndex == 1)
                OWAS_4 = 2;
            else if (selectedIndex == 2)
                OWAS_4 = 3;
        }

        # endregion

#region Methoden
private void Start_XYZ_Methode()
{
            XYZ_aufzeichnen = 1;               
}

private void Stop_XYZ_Methode()
{
            XYZ_aufzeichnen = 0;
            
            string XYZ_ExpLoc = save_path + "\\" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + " " + "Positionsdaten_EXPORT.csv";
            Position_export.ExportToFile(XYZ_ExpLoc);         
}

private void DynStart_Methode()
{          
    dyn_aufzeichnen = 1;       
}

private void DynStop_Methode()
{
            dyn_aufzeichnen = 0;
            string dynExpLoc = save_path + "\\" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + " " + "Gelenkwinkel_EXPORT.csv";
            Gelenkwinkel_export.ExportToFile(dynExpLoc);                 
}

private void OwasStart_Methode()
{
            OWAS_aufzeichnen = 1;
}

private void OwasStop_Methode()
{
            OWAS_aufzeichnen = 0;
            
            string OwasExpLoc = save_path + "\\" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + " " + "OWAS_EXPORT.csv";
            Owas_export.ExportToFile(OwasExpLoc);           
}

private void Capture_Count_START_Methode()
{
       Info5.Text = "Aufzeichung gestartet" + "\r\n" + "Captured Frames: " + Capture_FrameCount.Count;
}

private void Capture_Count_STOP_Methode()
{
    Info5.Text = "Aufzeichung gestoppt" + "\r\n" + "Captured Frames: " + Capture_FrameCount.Count;
    Capture_FrameCount.Clear();
}
      

#endregion


      
    }
}
