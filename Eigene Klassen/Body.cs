﻿namespace Microsoft.Samples.Kinect.BodyBasics
{
    using Microsoft.Kinect;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Handles the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        public void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {

            bool dataReceived = false;
            //bool laufen = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    // DB hier wird die floorClipPlane definiert
                    floorClipPlane = bodyFrame.FloorClipPlane;
                    
                    // DB Winkel des Sensors ausrechnen, für Neigungs/Roll Korrektur in Rumpfwinkeln
                    Neigungswinkel = Math.Round(Math.Atan(floorClipPlane.Z / floorClipPlane.Y) * 180 / Math.PI, 4);
                    Rollwinkel = Math.Round(Math.Atan(floorClipPlane.X / floorClipPlane.Y) * 180 / Math.PI, 4);  
                    
                    // DB Höhe + Neigungs- und Rotationswinkel vom Kinect Sensor ausgeben
                    sensinf.Text = "Höhe Sensor [m]: " + floorClipPlane.W.ToString("0.00") + "\r\n" + "Neigungswinkel [°]: " + Neigungswinkel +  "\r\n" + "Rollwinkel [°]: " + Rollwinkel;;
                    
                   
                    if (this.bodies == null)
                    {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;

                }
            }


            if (dataReceived)
            {

                using (DrawingContext dc = this.drawingGroup.Open())
                {
                    // Draw a transparent background to set the render size
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                    int penIndex = 0;

                    // Alle Personon tracken (foreach) oder nur die erste Person 
                    foreach (Body body in this.bodies)
                    //var body = bodies[0]; 

                    {
                        Pen drawPen = this.bodyColors[penIndex++];

                        if (body.IsTracked)
                        {
                            this.DrawClippedEdges(body, dc);

                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                            foreach (JointType jointType in joints.Keys)
                            {

                                //  
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;

                                if (position.Z < 0)
                                {

                                    position.Z = InferredZPositionClamp;

                                }

                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);

                            }


                            this.DrawBody(joints, jointPoints, dc, drawPen);

                            this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                            this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                            // prevent drawing outside of our render area
                            this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));


                            // DB Joint Definitions, Joint Positions to String
                            //Torso
                            //Joint Head = body.Joints[JointType.Head];
                            //Joint Spine_Shoulder = body.Joints[JointType.SpineShoulder];
                            //Joint Spine_Mid = body.Joints[JointType.SpineMid];
                            //Joint Spine_Base = body.Joints[JointType.SpineBase];
                            //Right Leg
                            //Joint Hip_Right = body.Joints[JointType.HipRight];
                            //Joint Knee_Right = body.Joints[JointType.KneeRight];
                            //Joint Ankle_Right = body.Joints[JointType.AnkleRight];
                            //Joint Foot_Right = body.Joints[JointType.FootRight];
                            //Left Leg
                            //Joint Hip_Left = body.Joints[JointType.HipLeft];
                            //Joint Knee_Left = body.Joints[JointType.KneeLeft];
                            //Joint Ankle_Left = body.Joints[JointType.AnkleLeft];
                            //Joint Foot_Left = body.Joints[JointType.FootLeft];
                            //Right Arm
                            //Joint Shoulder_Right = body.Joints[JointType.ShoulderRight];
                            //Joint Elbow_Right = body.Joints[JointType.ElbowRight];
                            //Joint Wrist_Right = body.Joints[JointType.WristRight];
                            //Joint Hand_Right = body.Joints[JointType.HandRight];
                            //Left Arm
                            //Joint Shoulder_Left = body.Joints[JointType.ShoulderLeft];
                            //Joint Elbow_Left = body.Joints[JointType.ElbowLeft];
                            //Joint Wrist_Left = body.Joints[JointType.WristLeft];
                            //Joint Hand_Left = body.Joints[JointType.HandLeft];
                            //Hand + Neck (additional since Kinect 2)
                            //Joint Hand_Tip_Right = body.Joints[JointType.HandTipRight];
                            //Joint Thumb_Right = body.Joints[JointType.ThumbRight];
                            //Joint Hand_Tip_Left = body.Joints[JointType.HandTipLeft];
                            //Joint Thumb_Left = body.Joints[JointType.ThumbLeft];
                            //Joint Neck = body.Joints[JointType.Neck];

                           

                            //Torso
                            Joint head = FloorCorrected(body.Joints[JointType.Head]);
                            Joint spineShoulder = FloorCorrected(body.Joints[JointType.SpineShoulder]);
                            Joint spineMid = FloorCorrected(body.Joints[JointType.SpineMid]);
                            Joint spineBase = FloorCorrected(body.Joints[JointType.SpineBase]);
                            //Right Leg
                            Joint hipRight = FloorCorrected(body.Joints[JointType.HipRight]);
                            Joint kneeRight = FloorCorrected(body.Joints[JointType.KneeRight]);
                            Joint ankleRight = FloorCorrected(body.Joints[JointType.AnkleRight]);
                            Joint footRight = FloorCorrected(body.Joints[JointType.FootRight]);
                            //Left Leg
                            Joint hipLeft = FloorCorrected(body.Joints[JointType.HipLeft]);
                            Joint kneeLeft = FloorCorrected(body.Joints[JointType.KneeLeft]);
                            Joint ankleLeft = FloorCorrected(body.Joints[JointType.AnkleLeft]);
                            Joint footLeft = FloorCorrected(body.Joints[JointType.FootLeft]);
                            //Right Arm
                            Joint shoulderRight = FloorCorrected(body.Joints[JointType.ShoulderRight]);
                            Joint elbowRight = FloorCorrected(body.Joints[JointType.ElbowRight]);
                            Joint wristRight = FloorCorrected(body.Joints[JointType.WristRight]);
                            Joint handRight = FloorCorrected(body.Joints[JointType.HandRight]);
                            //Left Arm
                            Joint shoulderLeft = FloorCorrected(body.Joints[JointType.ShoulderLeft]);
                            Joint elbowLeft = FloorCorrected(body.Joints[JointType.ElbowLeft]);
                            Joint wristLeft = FloorCorrected(body.Joints[JointType.WristLeft]);
                            Joint handLeft = FloorCorrected(body.Joints[JointType.HandLeft]);
                            //Hand + Neck (additional since Kinect 2)
                            Joint handTipRight = FloorCorrected(body.Joints[JointType.HandTipRight]);
                            Joint thumbRight = FloorCorrected(body.Joints[JointType.ThumbRight]);
                            Joint handTipLeft = FloorCorrected(body.Joints[JointType.HandTipLeft]);
                            Joint thumbLeft = FloorCorrected(body.Joints[JointType.ThumbLeft]);
                            Joint neck = FloorCorrected(body.Joints[JointType.Neck]);

                            datum = DateTime.Now.ToString("yyyy-MM-dd");
                            zeitstempel = DateTime.Now.ToString("HH:mm:ss,fff");

                            if (All_aufzeichnen == 1)
                            {
                                Capture_FrameCount.Add(zeitstempel);
                                Capture_Count_START_Methode();
                            }

                            #region XYZ Position CSV Export
                            // XYZ positions and tracking state to CSV
 
                            if (XYZ_aufzeichnen == 1)                              
                            {
                                Capture_Count_START_Methode();

                                if (All_aufzeichnen == 0)
                                {
                                    Capture_FrameCount.Add(zeitstempel);
                                }

                                Position_export.AddRow();

                                Position_export["Datum"] = datum;
                                Position_export["Zeitstempel"] = zeitstempel;

                                Position_export["Head.X"] = head.Position.X;
                                Position_export["Head.Y"] = head.Position.Y;
                                Position_export["Head.Z"] = head.Position.Z;
                                
                                Position_export["Spine_Shoulder.X"] = spineShoulder.Position.X;
                                Position_export["Spine_Shoulder.Y"] = spineShoulder.Position.Y;
                                Position_export["Spine_Shoulder.Z"] = spineShoulder.Position.Z;
                                
                                Position_export["Spine_Mid.X"] = spineMid.Position.X;
                                Position_export["Spine_Mid.Y"] = spineMid.Position.Y;
                                Position_export["Spine_Mid.Z"] = spineMid.Position.Z;
                                
                                Position_export["Spine_Base.X"] = spineBase.Position.X;
                                Position_export["Spine_Base.Y"] = spineBase.Position.Y;
                                Position_export["Spine_Base.Z"] = spineBase.Position.Z;

                                Position_export["Hip_Right.X"] = hipRight.Position.X;
                                Position_export["Hip_Right.Y"] = hipRight.Position.Y;
                                Position_export["Hip_Right.Z"] = hipRight.Position.Z;

                                Position_export["Knee_Right.X"] = kneeRight.Position.X;
                                Position_export["Knee_Right.Y"] = kneeRight.Position.Y;
                                Position_export["Knee_Right.Z"] = kneeRight.Position.Z;

                                Position_export["Ankle_Right.X"] = ankleRight.Position.X;
                                Position_export["Ankle_Right.Y"] = ankleRight.Position.Y;
                                Position_export["Ankle_Right.Z"] = ankleRight.Position.Z;

                                Position_export["Foot_Right.X"] = footRight.Position.X;
                                Position_export["Foot_Right.Y"] = footRight.Position.Y;
                                Position_export["Foot_Right.Z"] = footRight.Position.Z;

                                Position_export["Hip_Left.X"] = hipLeft.Position.X;
                                Position_export["Hip_Left.Y"] = hipLeft.Position.Y;
                                Position_export["Hip_Left.Z"] = hipLeft.Position.Z;

                                Position_export["Knee_Left.X"] = kneeLeft.Position.X;
                                Position_export["Knee_Left.Y"] = kneeLeft.Position.Y;
                                Position_export["Knee_Left.Z"] = kneeLeft.Position.Z;

                                Position_export["Ankle_Left.X"] = ankleLeft.Position.X;
                                Position_export["Ankle_Left.Y"] = ankleLeft.Position.Y;
                                Position_export["Ankle_Left.Z"] = ankleLeft.Position.Z;

                                Position_export["Foot_Left.X"] = footLeft.Position.X;
                                Position_export["Foot_Left.Y"] = footLeft.Position.Y;
                                Position_export["Foot_Left.Z"] = footLeft.Position.Z;

                                Position_export["Shoulder_Right.X"] = shoulderRight.Position.X;
                                Position_export["Shoulder_Right.Y"] = shoulderRight.Position.Y;
                                Position_export["Shoulder_Right.Z"] = shoulderRight.Position.Z;

                                Position_export["Elbow_Right.X"] = elbowRight.Position.X;
                                Position_export["Elbow_Right.Y"] = elbowRight.Position.Y;
                                Position_export["Elbow_Right.Z"] = elbowRight.Position.Z;

                                Position_export["Wrist_Right.X"] = wristRight.Position.X;
                                Position_export["Wrist_Right.Y"] = wristRight.Position.Y;
                                Position_export["Wrist_Right.Z"] = wristRight.Position.Z;

                                Position_export["Hand_Right.X"] = handRight.Position.X;
                                Position_export["Hand_Right.Y"] = handRight.Position.Y;
                                Position_export["Hand_Right.Z"] = handRight.Position.Z;

                                Position_export["Shoulder_Left.X"] = shoulderLeft.Position.X;
                                Position_export["Shoulder_Left.Y"] = shoulderLeft.Position.Y;
                                Position_export["Shoulder_Left.Z"] = shoulderLeft.Position.Z;

                                Position_export["Elbow_Left.X"] = elbowLeft.Position.X;
                                Position_export["Elbow_Left.Y"] = elbowLeft.Position.Y;
                                Position_export["Elbow_Left.Z"] = elbowLeft.Position.Z;

                                Position_export["Wrist_Left.X"] = wristLeft.Position.X;
                                Position_export["Wrist_Left.Y"] = wristLeft.Position.Y;
                                Position_export["Wrist_Left.Z"] = wristLeft.Position.Z;

                                Position_export["Hand_Left.X"] = handLeft.Position.X;
                                Position_export["Hand_Left.Y"] = handLeft.Position.Y;
                                Position_export["Hand_Left.Z"] = handLeft.Position.Z;

                                Position_export["Hand_Tip_Right.X"] = handTipRight.Position.X;
                                Position_export["Hand_Tip_Right.Y"] = handTipRight.Position.Y;
                                Position_export["Hand_Tip_Right.Z"] = handTipRight.Position.Z;

                                Position_export["Thumb_Right.X"] = thumbRight.Position.X;
                                Position_export["Thumb_Right.Y"] = thumbRight.Position.Y;
                                Position_export["Thumb_Right.Z"] = thumbRight.Position.Z;

                                Position_export["Hand_Tip_Left.X"] = handTipLeft.Position.X;
                                Position_export["Hand_Tip_Left.Y"] = handTipLeft.Position.Y;
                                Position_export["Hand_Tip_Left.Z"] = handTipLeft.Position.Z;

                                Position_export["Thumb_Left.X"] = thumbLeft.Position.X;
                                Position_export["Thumb_Left.Y"] = thumbLeft.Position.Y;
                                Position_export["Thumb_Left.Z"] = thumbLeft.Position.Z;

                                Position_export["Neck.X"] = neck.Position.X;
                                Position_export["Neck.Y"] = neck.Position.Y;
                                Position_export["Neck.Z"] = neck.Position.Z;

                                Position_export["Head Tracking State"] = head.TrackingState;
                                Position_export["Spine_Shoulder Tracking State"] = spineShoulder.TrackingState;
                                Position_export["Spine_Mid Tracking State"] = spineMid.TrackingState;
                                Position_export["Spine_Base Tracking State"] = spineBase.TrackingState;
                                Position_export["Hip_Right Tracking State"] = hipRight.TrackingState;
                                Position_export["Knee_Right Tracking State"] = kneeRight.TrackingState;
                                Position_export["Ankle_Right Tracking State"] = ankleRight.TrackingState;
                                Position_export["Foot_Right Tracking State"] = footRight.TrackingState;
                                Position_export["Hip_Left Tracking State"] = hipLeft.TrackingState;
                                Position_export["Knee_Left Tracking State"] = kneeLeft.TrackingState;
                                Position_export["Ankle_Left Tracking State"] = ankleLeft.TrackingState;
                                Position_export["Foot_Left Tracking State"] = footLeft.TrackingState;
                                Position_export["Shoulder_Right Tracking State"] = shoulderRight.TrackingState;
                                Position_export["Elbow_Right Tracking State"] = elbowRight.TrackingState;
                                Position_export["Wrist_Right Tracking State"] = wristRight.TrackingState;
                                Position_export["Hand_Right Tracking State"] = handRight.TrackingState;
                                Position_export["Shoulder_Left Tracking State"] = shoulderLeft.TrackingState;
                                Position_export["Elbow_Left Tracking State"] = elbowLeft.TrackingState;
                                Position_export["Wrist_Left Tracking State"] = wristLeft.TrackingState;
                                Position_export["Hand_Left Tracking State"] = handLeft.TrackingState;
                                Position_export["Hand_Tip_Right Tracking State"] = handTipRight.TrackingState;
                                Position_export["Thumb_Right Tracking State"] = thumbRight.TrackingState;
                                Position_export["Hand_Tip_Left Tracking State"] = handTipLeft.TrackingState;
                                Position_export["Thumb_Left Tracking State"] = thumbLeft.TrackingState;
                                Position_export["Neck Tracking State"] = neck.TrackingState;

                                
                            }
                            #endregion

                   
                        #region DB Gelenkwinkel

                            // DB Rumpfwinkel                                      
                            double u1 = hipLeft.Position.X - hipRight.Position.X;
                            double u2 = hipLeft.Position.Y - hipRight.Position.Y;
                            double u3 = hipLeft.Position.Z - hipRight.Position.Z;

                            double v1 = hipLeft.Position.X - hipRight.Position.X;
                            double v2 = (hipLeft.Position.Y + 0.5) - hipRight.Position.Y;
                            double v3 = hipLeft.Position.Z - hipRight.Position.Z;
                            
                            Vector3D ebene_long_normalenvektor = new Vector3D(u2 * v3 - u3 * v2, u3 * v1 - u1 * v3, u1 * v2 - u2 * v1);
                            Vector3D ebene_trans_normalenvektor = new Vector3D(ebene_long_normalenvektor.Z, ebene_long_normalenvektor.Y, ebene_long_normalenvektor.X);
                            Vector3D achse_center_huefte_schulter = new Vector3D(spineShoulder.Position.X - spineBase.Position.X, spineShoulder.Position.Y - spineBase.Position.Y, spineShoulder.Position.Z - spineBase.Position.Z);

                            // Projektion der Transversalachse auf die x-z Ebene für Rumpfrotation:
                            Vector huefte2D = new Vector(hipLeft.Position.X - hipRight.Position.X, hipLeft.Position.Z - hipRight.Position.Z);
                            Vector schulter2D = new Vector(shoulderLeft.Position.X - shoulderRight.Position.X, shoulderLeft.Position.Z - shoulderRight.Position.Z);

                            
                            
                            // DB Rumpfwinkel berechnung
                            koerperbeugung = Vector3D.AngleBetween(ebene_long_normalenvektor, achse_center_huefte_schulter) - 90 + Math.Abs(Neigungswinkel); //inkl. Neigungswinkelkorrektur
                            koerperneigung = Vector3D.AngleBetween(ebene_trans_normalenvektor, achse_center_huefte_schulter) - 90 - Math.Abs(Rollwinkel);
                            koerperrotation = Vector.AngleBetween(huefte2D, schulter2D);

                            // vermeiden negativer Anzeigewerte
                            if (koerperbeugung < 0)
                                koerperbeugung = koerperbeugung * (-1);
                            if (koerperneigung < 0)
                                koerperneigung = koerperneigung * (-1);
                            if (koerperrotation < 0)
                                koerperrotation = koerperrotation * (-1);

                            //Anzeige der Winkel in "innerGrid" oben links
                            Rumpf_beug.Text = (Math.Round(koerperbeugung / 5, MidpointRounding.AwayFromZero) * 5).ToString("0");
                            Rumpf_neig.Text = (Math.Round(koerperneigung / 5, MidpointRounding.AwayFromZero) * 5).ToString("0");
                            Rumpf_rot.Text = koerperrotation.ToString("0");

                        // DB Ellbogenwinkel 
                       
                            Vector3D ellenbogen_hand_re = new Vector3D(wristRight.Position.X - elbowRight.Position.X, wristRight.Position.Y - elbowRight.Position.Y, wristRight.Position.Z - elbowRight.Position.Z);
                            Vector3D ellenbogen_schulter_re = new Vector3D(shoulderRight.Position.X - elbowRight.Position.X, shoulderRight.Position.Y - elbowRight.Position.Y, shoulderRight.Position.Z - elbowRight.Position.Z);
                            Vector3D ellenbogen_hand_li = new Vector3D(handLeft.Position.X - elbowLeft.Position.X, handLeft.Position.Y - elbowLeft.Position.Y, handLeft.Position.Z - elbowLeft.Position.Z);
                            Vector3D ellenbogen_schulter_li = new Vector3D(shoulderLeft.Position.X - elbowLeft.Position.X, shoulderLeft.Position.Y - elbowLeft.Position.Y, shoulderLeft.Position.Z - elbowLeft.Position.Z);

                            ellenbogenwinkel_rechts = Vector3D.AngleBetween(ellenbogen_hand_re, ellenbogen_schulter_re);
                            ellenbogenwinkel_links = Vector3D.AngleBetween(ellenbogen_hand_li, ellenbogen_schulter_li);

                            //Anzeige Ellbogenwinkel in "inner grid" oben links
                            Ellbogenwinkel_re.Text = (Math.Round(ellenbogenwinkel_rechts / 5, MidpointRounding.AwayFromZero) * 5).ToString("0");
                            Ellbogenwinkel_li.Text = (Math.Round(ellenbogenwinkel_links / 5, MidpointRounding.AwayFromZero) * 5).ToString("0");
                                                 

                        // DB Kniewinkel     

                            Vector3D links_huefte_knie = new Vector3D(hipLeft.Position.X - kneeLeft.Position.X, hipLeft.Position.Y - kneeLeft.Position.Y, hipLeft.Position.Z - kneeLeft.Position.Z);
                            Vector3D rechts_huefte_knie = new Vector3D(hipRight.Position.X - kneeRight.Position.X, hipRight.Position.Y - kneeRight.Position.Y, hipRight.Position.Z - kneeRight.Position.Z);
                            Vector3D links_knie_fuß = new Vector3D(footLeft.Position.X - kneeLeft.Position.X, footLeft.Position.Y - kneeLeft.Position.Y, footLeft.Position.Z - kneeLeft.Position.Z);
                            Vector3D rechts_knie_fuß = new Vector3D(footRight.Position.X - kneeRight.Position.X, footRight.Position.Y - kneeRight.Position.Y, footRight.Position.Z - kneeRight.Position.Z);

                            // Kniewinkel: Knie gestreckt = 0°
                            kniewinkel_links = 180 - Vector3D.AngleBetween(links_knie_fuß, links_huefte_knie);
                            kniewinkel_rechts = 180 - Vector3D.AngleBetween(rechts_knie_fuß, rechts_huefte_knie);

                            //Anzeige in Viewbox "oben links"
                            Kniewinkel_li.Text = (Math.Round(kniewinkel_links / 5, MidpointRounding.AwayFromZero) * 5).ToString("0");
                            Kniewinkel_re.Text = (Math.Round(kniewinkel_rechts / 5, MidpointRounding.AwayFromZero) * 5).ToString("0");
                           
                                                        
                        // Gelenkwinkel Export
                           
                          
                           
                           if (dyn_aufzeichnen == 1)
                           {
                               Capture_Count_START_Methode();
                               
                               if (All_aufzeichnen == 0)
                               {
                                   Capture_FrameCount.Add(zeitstempel);
                               }

                            Gelenkwinkel_export.AddRow();
                            Gelenkwinkel_export["Datum"] = datum;
                            Gelenkwinkel_export["Zeitstempel"] = zeitstempel;
                            
                            Gelenkwinkel_export["Rumpfbeugung"] = koerperbeugung;
                            Gelenkwinkel_export["Rumpfneigung"] = koerperneigung;
                            Gelenkwinkel_export["Rumpfrotation"] = koerperrotation;
                            
                            Gelenkwinkel_export["Ellbogenwinkel li"] = ellenbogenwinkel_links;
                            Gelenkwinkel_export["Ellbogenwinekl re"] = ellenbogenwinkel_rechts;
                            
                            Gelenkwinkel_export["Kniewinkel li"] = kniewinkel_links;
                            Gelenkwinkel_export["Kniewinkel re"] = kniewinkel_rechts;

                          if (Capture_FrameCount.Count == 1800)
                            {
                                DynStop_Methode();
                            }

                           }                                         

                   #endregion Gelenkwinkel

                            # region DB_OWAS

                            #region OWAS 1                                    
                                                       
                            //Abfrage OWAS_1 = 1 (Oberkörper gerade)
                            if (koerperbeugung < 15 && koerperneigung < 15 && koerperrotation < 10)
                                OWAS_1 = 1;
                            else
                                //Abfrage OWAS_1 = 2 (Gebeugt -nach vorn oder nach hinten)
                                if (koerperbeugung > 15 && koerperneigung < 15 && koerperrotation < 10)
                                    OWAS_1 = 2;
                                else
                                    //Abfrage OWAS_1 = 3 (Gedreht, oder zur Seite geneigt)
                                    if ((koerperneigung > 15 | koerperrotation > 10) && koerperbeugung < 15)
                                        OWAS_1 = 3;
                                    else
                                        //Abfrage OWAS_1 = 4 (gebeugt UND gedreht oder nach vorn und zur Seite gebeugt
                                        if (koerperbeugung > 15 && koerperneigung > 15 || koerperbeugung > 15 && koerperrotation > 10 || koerperrotation > 10 && koerperneigung > 15)
                                            OWAS_1 = 4;
                            #endregion

                            #region OWAS 2
                            // OWAS_2: Arme --------------------------------------------------------------
                            // 1= beide Hände unter Schulterhöhe, 2= eine Hand über Schulterhöhe, 3= beide Hände über Schulterhöhe
                            if (handRight.Position.Y > shoulderRight.Position.Y & handLeft.Position.Y > shoulderLeft.Position.Y)
                            {
                                OWAS_2 = 3;
                            }
                            else if (handRight.Position.Y > shoulderRight.Position.Y | handLeft.Position.Y > shoulderLeft.Position.Y)
                            {
                                OWAS_2 = 2;
                            }
                            else
                            {
                                OWAS_2 = 1;
                            }
                            #endregion

                            #region OWAS3
                            
                            // Abfrage OWAS_3 = 1 (sitzend, checkbox) 
                            if (Sitzen == 1)
                                OWAS_3 = 1;
                            else
                                if (kniewinkel_links < 30 && kniewinkel_rechts < 30)
                                    OWAS_3 = 2;
                                else
                                    // Abfrage OWAS_3 = 3 (stehen auf einem Bein)
                                    if (kniewinkel_rechts < 30 && footLeft.Position.Y > (footRight.Position.Y + 0.05) || kniewinkel_links < 30 && footRight.Position.Y > (footLeft.Position.Y + 0.05))
                                        OWAS_3 = 3;
                                    else
                                        // Abfrage OWAS_3 = 4 (beide beine gebeugt, stehen oder kauern) 
                                        if (kniewinkel_links > 30 && kniewinkel_rechts > 30 && Math.Abs(footRight.Position.Y - footLeft.Position.Y) < 0.1) //Math.Abs liefert den Betrag, daher funktionierts für beide Seiten
                                            OWAS_3 = 4;
                                        else
                                            // Abfrage OWAS_3 = 5 (stehen oder kauern auf einem gebeugten Bein)
                                            if (kniewinkel_rechts > 30 && footLeft.Position.Y > (footRight.Position.Y + 0.05) || kniewinkel_links > 30 && footRight.Position.Y > (footLeft.Position.Y + 0.05))
                                                OWAS_3 = 5;
                                            else
                                                //Abfrage OWAS_3 = 6 (Knien auf einem oder beiden Knien)
                                                if ((kneeLeft.Position.Y < 0.15 || kneeRight.Position.Y < 0.15) || (kneeLeft.Position.Y < 0.15 && kneeRight.Position.Y < 0.15))
                                                    OWAS_3 = 6;

                            // OWAS_3_7
                            spine_points.Enqueue(spineBase);
                            if (spine_points.Count > 30)
                            {
                                var reference = spine_points.Dequeue();
                                var abstand = Distance(reference.Position, spineBase.Position);
                                if (abstand > 0.25)
                                {
                                    //OWAS_3_7 = 7;
                                    OWAS_3 = 7;
                                }
                            }

                            #endregion

                            #region OWAS 4
                            //OWAS_4 Last ---------------------------------------------------------------------- 
                            //Wird über Dropdown von Owas_ComboBox eingefügt
                            #endregion

                            #region OWAS_MK
                            // Zuordnung des Owas Codes zu einer Maßnahmenklasse aus Dictionary dict:
                            if (dict.ContainsKey(OWAS_Code) == true)
                            {
                                OWAS_MK = dict[OWAS_Code].ToString();
                            }

                            //Display Owas Code in TextBlock Info4        
                            OWAS_Code = OWAS_1.ToString() + OWAS_2.ToString() + OWAS_3.ToString() + OWAS_4.ToString();
                            Info4.Text = OWAS_Code;

                            // Display Maßnahmenklasse in MK_info + farbliche Zuordnung
                            MK_Info.Text = OWAS_MK;
                           
                            if (OWAS_MK == "1")
                            {
                                MK_Info1.Background = Brushes.LightGreen;
                                MK_Info2.Background = Brushes.LightGray;
                                MK_Info3.Background = Brushes.LightGray;
                                MK_Info4.Background = Brushes.LightGray;
                            }
                            
                            else if (OWAS_MK == "2")
                            {
                                MK_Info1.Background = Brushes.LightGreen;
                                MK_Info2.Background = Brushes.Yellow;
                                MK_Info3.Background = Brushes.LightGray;
                                MK_Info4.Background = Brushes.LightGray;
                            }
                                
                            else if (OWAS_MK == "3")
                            {
                                MK_Info1.Background = Brushes.LightGreen;
                                MK_Info2.Background = Brushes.Yellow;
                                MK_Info3.Background = Brushes.Orange;
                                MK_Info4.Background = Brushes.LightGray;
                            }
                               
                            else if (OWAS_MK == "4")
                            {
                                MK_Info1.Background = Brushes.LightGreen;
                                MK_Info2.Background = Brushes.Yellow;
                                MK_Info3.Background = Brushes.Orange;
                                MK_Info4.Background = Brushes.Red;
                            }
                               
                                                        
                            #endregion

                            #region Verbesserungsvorschläge

                           
                            if (Vorschläge == 1)
                            {
                                // Verbesserungsvorschlag Anzeigen
                                Verb_Info.Text = OWAS_Verb;
                            }
                                //Bei allen MK >1 wird dict2 durchsucht und bei einem Treffer die entsprechende Maßnahme angezeigt
                                if (dict2.ContainsKey(OWAS_Code) == true)
                                {
                                    OWAS_Verb = dict2[OWAS_Code].ToString();
                                }
                                
                            #endregion

                            #region OWAS_Export
                            
                            //OWAS *.csv Export aus klasse Csv Export.cs
                           
                          if (OWAS_aufzeichnen == 1)
                          {
                              Capture_Count_START_Methode();

                              if (All_aufzeichnen == 0)
                              {
                                  Capture_FrameCount.Add(zeitstempel);
                              }

                            Owas_export.AddRow();
                            Owas_export["Datum"] = datum;
                            Owas_export["Zeit"] = zeitstempel;
                            Owas_export["Owas Code"] = OWAS_Code;
                            Owas_export["Maßnahmenklasse"] = OWAS_MK;
                            Owas_export["Verbesserungsvorschläge"] = OWAS_Verb;
                          }

                            #endregion


                            #endregion

                        }
                    }
                }
            }
        }
        


        private Joint FloorCorrected(Joint joint)
        {
            return joint;

             //TODO: Floor correction
           /* var Position_origin = new Vector3D (joint.Position.X, joint.Position.Y, joint.Position.Z);

            var correctedPosition_rot =   Position_origin * rotMatrix ;
            var correctedPosition = correctedPosition_rot * translationMatrix;
           
            var correctedJoint = new Joint() { JointType = joint.JointType, Position= new CameraSpacePoint { X = (float)correctedPosition_rot.X, Y = (float)correctedPosition_rot.Y, Z = (float)correctedPosition_rot.Z }, TrackingState = joint.TrackingState };
            // Info5.Text = string.Format("ORG: {0}, {1}, {2}\nCOR: {3}, {4}, {5}", joint.Position.X, joint.Position.Y, joint.Position.Z, correctedPosition_rot.X, correctedPosition_rot.Y, correctedPosition_rot.Z);
            return correctedJoint;*/
        }

        public double Distance(CameraSpacePoint point1, CameraSpacePoint point2)
        {
            return new Vector(point1.X - point2.X, point1.Z - point2.Z).Length;
        }

    }
}


