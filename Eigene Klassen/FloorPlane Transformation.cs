﻿ namespace Microsoft.Samples.Kinect.BodyBasics
{
    using Microsoft.Kinect;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>,
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        
        public void CalculateCameraTransform ()  // DB ggf. nicht public void sondern Matrix3D, 
                                        // dann aber statt "trans = ..."  "return rotMatrix * translationMatrix" nehmen
        {
                    Vector3D yNew = new Vector3D(floorClipPlane.X, floorClipPlane.Y, floorClipPlane.Z);
                    Vector3D zNew = new Vector3D(0, 1, -floorClipPlane.Y/floorClipPlane.Z);
                    zNew.Normalize();
                    Vector3D xNew = Vector3D.CrossProduct(yNew, zNew);

                    rotMatrix = new Matrix3D(xNew.X, xNew.Y, xNew.Z, 0,
                                                      yNew.X, yNew.Y, yNew.Z, 0,
                                                      zNew.X, zNew.Y, zNew.Z, 0,
                                                       0, 0, 0, 1);

                    translationMatrix = new Matrix3D(1, 0, 0, 0,
                                                              0, 1, 0, -floorClipPlane.W,
                                                              0, 0, 1, 0,
                                                              0, 0, 0, 1);

                   
                    
           
            // Transformation Matrix für "World Space" inkl. Sensor Neigung + Höhenkorrenktur ausgeben
                    //trans = rotMatrix * translationMatrix;

                   correctionMatrix = rotMatrix * translationMatrix;
          }

    }
}